<?php

namespace App\Http\Controllers;
use App\Models\posts_users;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function example()
    {
      /*--------------------------------------------------------------------------
      | Example Question
      |--------------------------------------------------------------------------
      |Below have 3 variables [ $x , $y , $z]
      |try to write a formula to make $sum equal to 100
      */

      $x = 30;
      $y = 10;
      $z = 5;

      ## Answer
      $sum = ($x - $y) * $z;
      dd($sum);
    }

    public function question_1()
    {
      /*--------------------------------------------------------------------------
      | Question 1
      |--------------------------------------------------------------------------
      |Below have 3 variables [ $a , $b , $c]
      |try to write a formula to make $sum equal to 200
      */

      $a = 400;
      $b = 600;
      $c = 5;

      ## Answer


    }

    public function question_2()
    {
      /*--------------------------------------------------------------------------
      | Question 2
      |--------------------------------------------------------------------------
      |Below have 2 variables [ $a , $b ]
      |tip : please using max() to find the largest amount
      */

      $a = 10;
      $b = 18;

      ## Answer

    }

    public function question_3()
    {
      /*--------------------------------------------------------------------------
      | Question 3
      |--------------------------------------------------------------------------
      |Below have 3 variables [ $a , $b , $c ]
      |tip : please using min() to find the smallest amount
      */

      $a = 10;
      $b = 18;
      $c = 5;

      ## Answer

    }

    public function question_4()
    {
      /*--------------------------------------------------------------------------
      | Question 4
      |--------------------------------------------------------------------------
      |Below have 4 variables [ $string , $string_2 , $string_3 ,  $string_4 ]
      |tip : please using string concatenation to display as "Hello, I love to coding and how about you?"
      */

      $string   = "Hello, I lo";
      $string_2 = "you?";
      $string_3 = "ve to coding";
      $string_4 = "and how about ";

      ## Answer

    }

    public function question_5()
    {
      /*--------------------------------------------------------------------------
      | Question 5
      |--------------------------------------------------------------------------
      |Below have 3 variables [ $string , $string_2, , $string_3, ]
      |tip : please using string concatenation and str_replace() to display as "Yes, I love coding too!"
      */

      $string = "too!";
      $string_2 = " love coding and";
      $string_3 = "Yes, I";

      ## Answer

    }

    public function question_6()
    {
      /*--------------------------------------------------------------------------
      | Question 6
      |--------------------------------------------------------------------------
      |Below have an $zodiac array with 3 items
      |tip : please using array_push to insert 1 more item [ "Leo" ] into $zodiac array
      */

      $zodiac = ["Aries", "Taurus", "Gemini"];

      ## Answer

    }

    public function question_7()
    {
      /*--------------------------------------------------------------------------
      | Question 7
      |--------------------------------------------------------------------------
      |Below have an $fruit associative array with the quantity
      |the $sum_kiwi_banana is 16 [ total quantity for kiwi and banana]
      |please provide the $total_quantity formula that return total 3 fruits quantity for [ apple, orange, grape]
      */

      $fruit = [
        "apple"  => 10,
        "kiwi"   => 9,
        "orange" => 3,
        "banana" => 7,
        "grape"  => 11,
      ];

      $sum_kiwi_banana = $fruit['kiwi'] + $fruit['banana']; // 16

      ## Answer


    }

    public function question_8()
    {
      /*--------------------------------------------------------------------------
      | Question 8
      |--------------------------------------------------------------------------
      |Below have an $fruit associative array with the quantity
      |using [for-loop or foreach-loop] to change the kiwi quantity from 9 -> 5
      |tip : please using if statement to detect the kiwi in the [for-loop or foreach-loop]
      */

      $fruit = [
        "apple"  => 10,
        "kiwi"   => 9,
        "orange" => 3,
      ];

      ## Answer

    }

    public function question_9()
    {
      /*--------------------------------------------------------------------------
      | Question 9
      |--------------------------------------------------------------------------
      |Below have 2 $animal arrays
      |please provide the $animal_array that contain all the animals from $animal and $animal_2 [ no duplicate animals]
      |tip : please using array_merge and array_unique
      */

      $animal   = [ "dog" , "cat" , "elephant" , "tiger" ];
      $animal_2 = [ "horse", "tiger", "rabbit"];

      ## Answer

    }

    public function question_10()
    {
      /*--------------------------------------------------------------------------
      | Question 10
      |--------------------------------------------------------------------------
      |Below have an $language array
      |Remove the English and Mandarin from the $language array
      |tip : please using unset()
      */

      $language   = [ "Indonesian" , "English" , "Mandarin" , "Thai" ];


      ## Answer

    }

    public function question_11()
    {
      /*--------------------------------------------------------------------------
      | Question 11
      |--------------------------------------------------------------------------
      |Below have a $string
      |Provide a $pencil variable that display "pencil" only
      |tip : please using explode()
      */

      $string   = "This is a pencil";

      ## Answer

    }

    public function question_12()
    {
      /*--------------------------------------------------------------------------
      | Question 12
      |--------------------------------------------------------------------------
      |Below have a $array array variable with 2 items
      |Provide a $question variable that display string "This is a pencil, is it?" from the $array
      |tip : please using implode()
      */

      $array   = ["This is a pencil", "is it?"];

      ## Answer

    }

    public function question_13()
    {
      /*--------------------------------------------------------------------------
      | Question 13
      |--------------------------------------------------------------------------
      |Below have 4 variables [ $a, $b, $c , $d]
      |
      | 3 examples LOGICAL condition is provided
      | ($a == $d) will return TRUE
      | ($a == $c) will return FALSE
      | ($a + $b == $c) will return FALSE
      |
      | Please provide a new LOGICAL condition that will return TRUE, with using all 4 variables
      */

      $a = 5;
      $b = 20;
      $c = 30;
      $d = 5;

      ## 3 examples LOGICAL condition
      ($a      == $d);      ## TRUE
      ($a      == $c);      ## FALSE
      ($a + $b == $c);      ## FALSE

      ## Answer

    }

    public function question_14()
    {
      /*--------------------------------------------------------------------------
      | Question 14
      |--------------------------------------------------------------------------
      |Below have 4 variables [ $a, $b, $c , $d ] that represent the LOGICAL condition
      |
      | 3 example logical condition with operator && (AND) , || (OR) is provided
      |
      | Please provide a new LOGICAL operator condition that will return TRUE, with using [ $b, $c and $d ] only
      | Both operator && (AND) , || (OR) are required
      */

      ## 3 logical condition
      $a = is_numeric(5);
      $b = ( 10 % 2 == 0);
      $c = is_numeric("test");
      $d = false;

      ## 3 example logical condition with operator && (AND) , || (OR)
      ($a && $b);      ## TRUE
      ($a && $c);      ## FALSE
      ($a || $c);      ## TRUE

      ## Answer

    }

    public function question_15()
    {
      /*--------------------------------------------------------------------------
      | Question 15
      |--------------------------------------------------------------------------
      | Please using foreach loop to scan the $array and restructure the data into $digit_array
      |
      |  Requirement - the $digit_array only can contain EVEN number that between 0 until 100, in ascending order
      |
      |  [ Expected outcome]
      |
      |   array:2 [▼
      |     0 => 56
      |     1 => 90
      |   ]
      |
      */

      $array = [ 197, 11, 90, 45, 39, 71, 1, "drink", 99, "eat", 175, 56];

      $digit_array = [];

      ## Answer

    }


}
