<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\QuestionController;

/*
|--------------------------------------------------------------------------
| 30 questions
|--------------------------------------------------------------------------
|
|Hello, please answer below 30 questions,
|the question can be found in the directory [app > Http > Controllers > QuestionController]
|
| EXAMPLE == Route::get('example', [QuestionController::class,'example']);
*/


Route::get('question_1', [QuestionController::class,'question_1']);
Route::get('question_2', [QuestionController::class,'question_2']);
Route::get('question_3', [QuestionController::class,'question_3']);
Route::get('question_4', [QuestionController::class,'question_4']);
Route::get('question_5', [QuestionController::class,'question_5']);
Route::get('question_6', [QuestionController::class,'question_6']);
Route::get('question_7', [QuestionController::class,'question_7']);
Route::get('question_8', [QuestionController::class,'question_8']);
Route::get('question_9', [QuestionController::class,'question_9']);
Route::get('question_10', [QuestionController::class,'question_10']);
Route::get('question_11', [QuestionController::class,'question_11']);
Route::get('question_12', [QuestionController::class,'question_12']);
Route::get('question_13', [QuestionController::class,'question_13']);
Route::get('question_14', [QuestionController::class,'question_14']);
Route::get('question_15', [QuestionController::class,'question_15']);
Route::get('question_16', [QuestionController::class,'question_16']);
Route::get('question_17', [QuestionController::class,'question_17']);
Route::get('question_18', [QuestionController::class,'question_18']);
Route::get('question_19', [QuestionController::class,'question_19']);
Route::get('question_20', [QuestionController::class,'question_20']);
Route::get('question_21', [QuestionController::class,'question_21']);
Route::get('question_22', [QuestionController::class,'question_22']);
Route::get('question_23', [QuestionController::class,'question_23']);
Route::get('question_24', [QuestionController::class,'question_24']);
Route::get('question_25', [QuestionController::class,'question_25']);
Route::get('question_26', [QuestionController::class,'question_26']);
Route::get('question_27', [QuestionController::class,'question_27']);
Route::get('question_28', [QuestionController::class,'question_28']);
Route::get('question_29', [QuestionController::class,'question_29']);
Route::get('question_30', [QuestionController::class,'question_30']);
